Pseudovelocity, local rate of change embedding for single cell mRNA expression
==============================================================================

## Summary
Pseudovelocity (PV) is an estimate of the local rate of change using the knn neighbourhood. The estimate of velocity is an alternative to that of RNA-velocity (RNAv) (La Manno et. al 2019) but can't be done denovo and require a local temporal axis.
Pseudovelocity uses the tools of [scvelo](https://github.com/theislab/scvelo) to visualize and project the inferred rates of change.

PV estimates the local rate of change expected for the observed expression levels and produces results comparable to RNAv in some aspect but differ in that PV tracks the expected direction of expression.
<p align="center"><img src="img/fig_repo/hgforebrainglut_tjarnberg_vs_Lamanno_expression_velocity_correlation_figure.svg" width="100%" /></p>


PV can also be used to estimate decay rates as a function expression and maximal observed negative PV. Here using the inferred cell cycle in Yeast from Jackson et. al. 2020.
<p align="center"><img src="img/fig_repo/Jackson2020_YPD_PV.svg" width="100%" /></p>

Install latest version by cloning this repository
```
git clone https://gitlab.com/Xparx/pseudovelocity.git
cd pseudovelocity
```
and then in the pseudovelocity directory:
```
pip install .
```

pseudovelocity relies on the scanpy AnnData format and requaire that format as input.
It also assumes that the data used is appropriately filtered and transformed (not shown).
```
adata = sc.read('/path/to/dataset.h5ad')
```

Import and pick a regression method (this may change)
```
import pseudovelocity as pv
from sklearn.linear_model import LinearRegression
```

To run an analysis make sure that neighbours are computed 
```
sc.pp.pca(adata)
sc.pp.neighbors(adata, n_neighbors=100, n_pcs=50)
```

```
regressf = LinearRegression
kwfit = {}

neighbors_key = 'neighbors'
expression_layer = None  # Use X by default.
velocity_layer = 'pseudovelocity'
pseudov = pv.PV(adata, iterations=0, verbose=True, predict_offset=False, use_layer=expression_layer, add_layer=velocity_layer, regressf=regressf, kwfit=kwfit, impute_velocity=True, neighbors_key=neighbors_key, max_neighbors=np.inf)
```
