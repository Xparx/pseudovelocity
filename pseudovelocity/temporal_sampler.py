import scanpy as _sc
from scanpy import AnnData as _AnnData
from scanpy import Neighbors as _Neighbors
# from scipy.sparse import issparse as _issparse
import numpy as _np
# import scipy.sparse as _scs
import pandas as _pd


def cluster_anchor_chain_from_dict(adata, order_dict, cluster_name):

    cell_anchors = assign_cluster_to_indices(adata.obs[cluster_name], adata.obs_names)

    anchor_dict = {}
    for k, v in order_dict.items():
        anchor_dict[cell_anchors[k]] = cell_anchors[v]

    adata.uns['anchors'] = anchor_dict


def find_the_centroid_cell_for_cluster(adata, cluster_label):
    """
    NOTE: May be unfinished un-tested.
    adata: AnnData object
    cluster_label: string for column in obs.
    """

    import scipy as sp

    clusters = adata.obs[cluster_label]
    if not isinstance(clusters, _pd.core.arrays.categorical.Categorical):
        clusters = _pd.core.arrays.categorical.Categorical(clusters)

    for i in clusters.categories.tolist():

        samplei = adata.obs_names[(adata.obs[cluster_label] == i)]
        X = adata[samplei].X.A.copy()
        cc = X.mean(0)

        dists = sp.spatial.distance.cdist(cc, X)

        return i, dists


def assign_cluster_to_indices(clusters, indices, choose=0):
    """Use a pandas categorical and a list of unique indices to map anchors to cells"""

    if not isinstance(clusters, _pd.core.arrays.categorical.Categorical):
        clusters = _pd.core.arrays.categorical.Categorical(clusters)

    cell_anchors = {}
    for i in clusters.categories.tolist():

        anchors = indices[(clusters == i).tolist()]
        choose = choose if not (choose is None) else _np.random.randint(len(anchors))
        anchor = anchors[choose]
        anchr = _np.where(indices == anchor)[0][0]
        cell_anchors[i] = anchr

    return cell_anchors


def generate_neighbors_from_anndata(adata,
                                    n_neighbors=None,
                                    iroot=None,
                                    anchors=None,
                                    neighbors_key='neighbors',
                                    n_comps=50,
                                    n_pcs=None,
                                    n_dcs=None,
                                    use_rep='X_pca',
                                    ):
    """
    Handle AnnData objects correctly.
    """

    if not isinstance(adata, _AnnData):
        adata = _sc.AnnData(adata)

    if ('iroot' in adata.uns_keys()) and (iroot is None):
        iroot = adata.uns['iroot'][0] if hasattr(adata.uns['iroot'], '__iter__') else adata.uns['iroot']
    else:
        adata.uns['iroot'] = iroot

    if ('anchors' in adata.uns_keys()) and (anchors is None):
        anchors = adata.uns['anchors'].copy()
    else:
        anchors = {iroot: iroot}

    if 'pca' in adata.uns_keys():
        neighbors = _Neighbors(adata, neighbors_key=neighbors_key)
    else:
        _sc.pp.pca(adata, n_comps)
        neighbors = _Neighbors(adata, neighbors_key=neighbors_key)

    if not hasattr(neighbors, 'n_neighbors'):  # Assume nothing as been precomputed
        n_default = 10 if (n_neighbors is None) else n_neighbors
        neighbors.compute_neighbors(n_neighbors=n_default, use_rep=use_rep, n_pcs=n_pcs)
        neighbors.compute_transitions()
        d_default = 15 if (n_dcs is None) else n_dcs
        neighbors.compute_eigen(n_comps=d_default)

    return neighbors, anchors


def get_neighbors(neighbors, i):
    """Get neighbor indices for cell i.

    :param i: cell of interest
    :param get_distances: also get the raw distances to neighbors. Not implemented yet.
    :returns: neighbor indices
    :rtype: numpy array
    """

    neighbor_indices = neighbors.distances[i, :]
    # Following line might require neighbors.distances to be Compressed Sparse Row format (default).
    neighbor_indices = neighbor_indices.indices  # .sum(0).nonzero()
    neighbor_indices = _np.unique(_np.append(i, neighbor_indices))
    # neighbor_indices = _np.unique(_np.append(i, neighbor_indices[1]))

    return neighbor_indices


def find_anchor_cell(neighbors, celli, anchors=None, within_clusters=None):
    """
    Find the upstream closest anchor to a specfic cell and return
    index of as well as annotation for downstream leaf node.

    """
    if anchors is not None:
        leafs = list(anchors.keys())
    else:
        leafs = [neighbors.iroot]
        anchors = {neighbors.iroot: neighbors.iroot}

    if within_clusters is None:
        celldists = neighbors.distances_dpt[celli]
        celldists = celldists / celldists.max()
        min_leaf_index = _np.argmin(celldists[leafs])  # Find closest leaf
        closest_leaf = leafs[min_leaf_index]  # Extract closest leaf cell

    else:  # DONE: Instead of using distances use clusters to estimate anchor and leaf.
        if not isinstance(within_clusters, _pd.core.arrays.categorical.Categorical):
            within_clusters = _pd.core.arrays.categorical.Categorical(within_clusters)

        cluster = within_clusters[celli]
        leafclusters = within_clusters[leafs]
        li = leafclusters.isin([cluster])
        closest_leaf = _np.array(leafs)[li][0]
        min_leaf_index = leafclusters[li][0]

    upstream_dpt_root = anchors[closest_leaf]  # extract upstream root

    return upstream_dpt_root, closest_leaf, min_leaf_index


def get_neighbor_distances(neighbors, neighbor_indices, celli,
                           anchors=None,
                           scale_max=True,
                           reverse=False,
                           max_neighbors=None,
                           within_clusters=None,
                           ):
    """
    Get the time order of cells. Cell delta times are extracted
    from the central cell while ordering is derived from the anchor cell.
    """

    if max_neighbors is None:
        max_neighbors = neighbors.n_neighbors

    iroot, __, __ = find_anchor_cell(neighbors, celli,
                                     anchors=anchors,
                                     within_clusters=within_clusters)

    t0 = neighbors.distances_dpt[iroot]

    t1 = neighbors.distances_dpt[celli]

    if scale_max:
        t0 = t0 / t0.max()
        t1 = t1 / t1.max()

    if reverse:
        deltat = t0[celli] - t0
    else:
        deltat = t0 - t0[celli]

    t1 = t1 * _np.sign(deltat)

    delta_times = t1[neighbor_indices]

    # if self.max_c:
    #     closest_indices = _np.argsort(_np.absolute(delta_times))[:min(maxn, len(neighbor_indices))]
    #     delta_times = delta_times[closest_indices]
    #     neighbor_indices = neighbor_indices[closest_indices]

    return (delta_times, neighbor_indices)


def extract_neighbor_data(data, neighbor_indices, subset_var=None, use_layer='X'):
    """
    Get neighbor data from array.
    """

    adata = data.raw if (use_layer == 'raw') else data

    use_variable = False
    if subset_var is not None:
        use_variable = True
        variables = [i for i in subset_var if i in adata.var_names]

    adata_comp = adata[:, variables] if use_variable else adata

    if use_layer == 'X' or use_layer == 'raw':
        X = adata_comp[neighbor_indices, :].X.copy()
    else:
        X = adata_comp[neighbor_indices, :].layers[use_layer].copy()

    return X


def extract_data_and_time(adata, neighbors, cell, max_neighbors=None):
    """
    Extracts expression values and pseudotime for neighbors of a specific cell.

    :param adata: AnnData object
    :param cell: what cell should be the center
    :returns: Y, x
    :rtype: numpy.array

    """

    if isinstance(cell, str):
        celli = _np.where(adata.obs_names == cell)[0][0]
    elif isinstance(cell, int):
        celli = cell
    else:
        raise ValueError(f"cell imput must be int or str. Currently its {type(cell)}")

    neighbor_indices = get_neighbors(neighbors, celli)

    x, neighbor_indices = get_neighbor_distances(neighbors, neighbor_indices, celli,
                                                 scale_max=True,
                                                 reverse=False,
                                                 max_neighbors=max_neighbors)

    Y = extract_neighbor_data(adata, neighbor_indices)

    return Y, x


def pick_from_strat(delta_times, neighbor_indices, strategy='farthest'):

    if strategy == 'farthest':
        return neighbor_indices[delta_times.argmax()]

    elif strategy == 'closest':
        return neighbor_indices[delta_times.argmin()]

    elif strategy == 'random':
        return _np.random.choice(neighbor_indices)

    else:
        print('No strategy match, picking first in list')
        return neighbor_indices[0]


def daisy_chain(start_cell, neighbors, strategy='farthest'):
    """
    Daisy chains time series together by strategy.
    Strategies are random, farthest, closest

    """

    n_futures = _np.inf

    curr_cell = start_cell

    chain = {}

    while n_futures > 0:
        n_futures = 0

        ni = get_neighbors(neighbors, curr_cell)

        delta_times, neighbor_indices = get_neighbor_distances(neighbors, ni, curr_cell)

        neighbor_indices = neighbor_indices[delta_times > 0]
        delta_times = delta_times[delta_times > 0]

        n_futures = neighbor_indices.shape[0]

        if n_futures == 0:
            return chain

        chain[curr_cell] = pick_from_strat(delta_times, neighbor_indices, strategy=strategy)

        curr_cell = chain[curr_cell]
