from setuptools import setup

DISTNAME = 'pseudovelocity'
VERSION = '0.11'
DESCRIPTION = "Derive local rate of change in single cell expression based on estimated temporal distances."
# with open('README.rst') as f:
#     LONG_DESCRIPTION = f.read()
MAINTAINER = 'Andreas Tjärnberg'
MAINTAINER_EMAIL = 'andreas.tjarnberg@nyu.edu'
URL = 'https://gitlab.com/Xparx/pseudovelocity'
LICENSE = 'LGPL'


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      url=URL,
      author=MAINTAINER,
      author_email=MAINTAINER_EMAIL,
      license=LICENSE,
      packages=['pseudovelocity'],
      python_requires='>=3.7',
      install_requires=[
          'numpy',
          'scikit-learn',
          'pandas',
          'scvelo',
      ],
      zip_safe=False)
